package com.example.facitqroccupancy;

import com.example.facitqroccupancy.models.loginModel;
import com.example.facitqroccupancy.models.signupModel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface interfaceApi {

    public static String BASE_URL="https://hanworthwindows.co.uk/door/";

            @GET("onefile.php?")
            Call<loginModel> login(@Query("email") String email,
                                   @Query("password") String password,
                                   @Query("function") String function
                       );





            @POST("onefile.php?")
            Call<signupModel> signup(@Query("first_name") String first_name,
                                     @Query("last_name") String last_name,
                                     @Query("email") String email,
                                     @Query("phone") Long phone,
                                     @Query("address") String address,
                                     @Query("company_name") String company_name,
                                     @Query("password") String password,
                                     @Query("function") String function,
                                     @Query("country_name") String country_name,
                                     @Query("postat_code") String postat_code

            );


}
