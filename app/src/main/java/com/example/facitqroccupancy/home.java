package com.example.facitqroccupancy;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.facitqroccupancy.dataClass.qrData;
import com.google.gson.Gson;
import com.google.zxing.Result;
import com.gpfreetech.awesomescanner.ui.GpCodeScanner;
import com.gpfreetech.awesomescanner.ui.ScannerView;
import com.gpfreetech.awesomescanner.util.DecodeCallback;
import com.mukesh.tinydb.TinyDB;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class home extends AppCompatActivity  {

        int in_Count = 0;
        int out_Count = 0;
        TinyDB tinyDB;
        SharedPreferences sharedPreferences_count;
        ArrayList list;
        TextView in_tv;
        TextView out_tv;
        ConstraintLayout main_layout;
        ImageView image_stop;

    private GpCodeScanner mCodeScanner;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        in_tv=findViewById(R.id.in_res);
        out_tv=findViewById(R.id.out_res);
        image_stop=findViewById(R.id.img_stop);
        main_layout=findViewById(R.id.main_layout);
        list=new ArrayList();
        get_in();
        get_out();
        set_stop();
        tinyDB=new TinyDB(getApplicationContext());
        list.add("8sd8as8da9");
        in_tv.setText(""+in_Count);
        out_tv.setText(""+out_Count);
        ScannerView scannerView = findViewById(R.id.scanner_view);
        mCodeScanner = new GpCodeScanner(this, scannerView,GpCodeScanner.CAMERA_BACK);
        list.add("test");
        for (int i = 0; i < tinyDB.getListString("qrData").size(); i++) {
            Log.v("sd8as98d7asd",tinyDB.getListString("qrData").get(i)+"\n");

        }

        mCodeScanner.setDecodeCallback(new DecodeCallback() {
            @Override
            public void onDecoded(@NonNull final Result rawResult) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mCodeScanner.startPreview();
                        if(get_qr_data(rawResult.getText())==true){
                            get_in();
                            get_out();
                            out_Count++;
                            in_Count--;
                            set_stop();
                            in_tv.setText(""+in_Count);
                            out_tv.setText(""+out_Count);
                            list.remove(rawResult.getText());
                            update_count();
                            saveListToPref();
                            Toast.makeText(getApplicationContext(),"data found and removed",Toast.LENGTH_SHORT).show();
                            mCodeScanner.stopPreview();
                            delay(1000);
                        }
                        else
                        {
                           list.add(rawResult.getText());
                                saveListToPref();
                                get_in();
                                in_Count++;
                                in_tv.setText(""+in_Count);
                                update_count();
                                set_stop();
                                mCodeScanner.stopPreview();
                                delay(1000);
                        }
                    }
                });
            }
        });

        scannerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCodeScanner.startPreview();
            }
        });
    }

                @Override
                protected void onResume() {
                    super.onResume();
                    if(mCodeScanner!=null) {
                        mCodeScanner.startPreview();
                    }
                }
                @Override
                protected void onPause() {
                    if(mCodeScanner!=null) {
                        mCodeScanner.releaseResources();
                    }
                    super.onPause();
                }




        boolean saveListToPref() {
           tinyDB.putListString("qrData",list);
           return true;
    }



        boolean get_qr_data(String searchData){
           for (int i = 0; i < tinyDB.getListString("qrData").size(); i++) {
               if (tinyDB.getListString("qrData").get(i).equals(searchData)) {
               return true;
         }
          }
            return false;
                }
                void delay(int delay){
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mCodeScanner.startPreview();

                        }
                    },delay);

                }
                boolean update_count() {
                    sharedPreferences_count = getSharedPreferences("count", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences_count.edit();
                    if(true){
                    editor.putString("in", ""+in_Count);
                    editor.putString("out", ""+out_Count);
                    editor.apply();
                    return true;
                }
                        return false;

                }
                void get_in(){

                    sharedPreferences_count = getSharedPreferences("count", MODE_PRIVATE);
                    if(sharedPreferences_count.getString("in","")!=null){

                      try {
                          in_Count=Integer.parseInt(sharedPreferences_count.getString("in",""));

                      }catch (Exception g){
                          in_Count=0;
                      }

                    }
                    else {

                    in_Count=0;
                    }

                        }
                void get_out(){
                    sharedPreferences_count = getSharedPreferences("count", MODE_PRIVATE);
                    if(sharedPreferences_count.getString("out","")!=null){
                        try {
                            out_Count=Integer.parseInt(sharedPreferences_count.getString("out",""));
                        }catch (Exception g){
                            out_Count=0;
                        }
                    }
                    else {
                        out_Count=0;
                    }
    }

    void set_stop(){
        if(in_Count>=10){
            image_stop.setVisibility(View.VISIBLE);
            main_layout.setBackgroundColor(Color.parseColor("#8B0000"));

        }else {
            image_stop.setVisibility(View.GONE);
            main_layout.setBackgroundColor(Color.parseColor("#00FF00"));
        }
    }


    }





