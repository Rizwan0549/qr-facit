package com.example.facitqroccupancy.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.facitqroccupancy.R;
import com.example.facitqroccupancy.home;
import com.example.facitqroccupancy.interfaceApi;
import com.example.facitqroccupancy.models.signupModel;
import com.google.android.material.textfield.TextInputEditText;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class signup extends Fragment {
   TextInputEditText tv_fname;
    TextInputEditText tv_lname;
    TextInputEditText tv_company_name;
    TextInputEditText tv_address;
    TextInputEditText tv_postcode;
    TextInputEditText tv_countryname;
    TextInputEditText tv_email1;
    TextInputEditText tv_email2;
    TextInputEditText tv_password1;
    TextInputEditText tv_password2;
    TextInputEditText tv_phno;
    Button btn_registration;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.fragment_signup, container, false);

        tv_fname=view.findViewById(R.id.fname);
        tv_lname=view.findViewById(R.id.lname);
        tv_company_name=view.findViewById(R.id.companyname);
        tv_address=view.findViewById(R.id.addressa);
        tv_postcode=view.findViewById(R.id.postcodea);
        tv_countryname=view.findViewById(R.id.conname);
        tv_email1=view.findViewById(R.id.emaila1);
        tv_email2=view.findViewById(R.id.emaila2);
        tv_password1=view.findViewById(R.id.passa1);
        tv_password2=view.findViewById(R.id.passa2);
        tv_phno=view.findViewById(R.id.phno);
        btn_registration=view.findViewById(R.id.btn_register);

btn_registration.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {

        Retrofit retrofit = new Retrofit
                .Builder()
                .baseUrl(interfaceApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        interfaceApi interfaceApi=retrofit.create(com.example.facitqroccupancy.interfaceApi.class);

        Call<signupModel> call=interfaceApi.signup(

                tv_fname.getText().toString(),
                tv_lname.getText().toString(),
                tv_email1.getText().toString(),
                Long.parseLong(tv_phno.getText().toString()),
                tv_address.getText().toString(),
                tv_company_name.getText().toString(),
                tv_password1.getText().toString(),
                "register",
                tv_countryname.getText().toString(),
                tv_postcode.getText().toString()
        );



        call.enqueue(new Callback<signupModel>() {
            @Override
            public void onResponse(Call<signupModel> call, Response<signupModel> response) {

                Toast.makeText(getActivity(),response.body().getMessage(),Toast.LENGTH_SHORT).show();
                if(response.body().getSuccess()==1){
                    Toast.makeText(getActivity(),response.body().getMessage(),Toast.LENGTH_SHORT).show();
                    Intent intent=new Intent(getActivity(), home.class);
                    startActivity(intent);
                }



            }

            @Override
            public void onFailure(Call<signupModel> call, Throwable t) {
                Log.v("error",t.getMessage());
            }
        });


    }
});



        return view;
    }
}
