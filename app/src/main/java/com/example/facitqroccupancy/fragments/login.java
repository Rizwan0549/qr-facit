package com.example.facitqroccupancy.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.facitqroccupancy.R;
import com.example.facitqroccupancy.home;
import com.example.facitqroccupancy.interfaceApi;
import com.example.facitqroccupancy.models.loginModel;
import com.google.android.material.textfield.TextInputEditText;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class login extends Fragment {

    Button btn_login;
    TextInputEditText email;
    TextInputEditText pass;
    ProgressDialog progressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.fragment_login, container, false);
        btn_login=view.findViewById(R.id.login);
        email=view.findViewById(R.id.email);
        pass=view.findViewById(R.id.pass);


        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Retrofit retrofit = new Retrofit
                        .Builder()
                        .baseUrl(interfaceApi.BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();


                //MyApiService service = MyApiService.retrofit.create(MyApiService.class);

                interfaceApi interfaceApi = retrofit.create(com.example.facitqroccupancy.interfaceApi.class);
                Call<loginModel> call = interfaceApi.login(email.getText().toString(), pass.getText().toString(), "login");


                call.enqueue(new Callback<loginModel>() {
                    @Override
                    public void onResponse(Call<loginModel> call, Response<loginModel> response) {
                       if(response.isSuccessful()){
                          if(response.body().getSuccess()==1){
                              Toast.makeText(getActivity(),"Login Succesfully",Toast.LENGTH_SHORT).show();
                              Intent intent=new Intent(getActivity(), home.class);
                              startActivity(intent);
                          }else
                              Toast.makeText(getActivity(),"Login Failed",Toast.LENGTH_SHORT).show();

                       }


                    }

                    @Override
                    public void onFailure(Call<loginModel> call, Throwable t) {
                        Log.v("rese",t.getMessage());
                    }
                });



            }
        });


        return view;


    }
}
