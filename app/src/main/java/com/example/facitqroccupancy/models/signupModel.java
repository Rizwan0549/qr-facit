package com.example.facitqroccupancy.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class signupModel {

    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("userDetail")
    @Expose
    private userDetailsModelSignup userDetail;

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public userDetailsModelSignup getUserDetail() {
        return userDetail;
    }

    public void setUserDetail(userDetailsModelSignup userDetail) {
        this.userDetail = userDetail;
    }
}