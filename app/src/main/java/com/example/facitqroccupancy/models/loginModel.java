package com.example.facitqroccupancy.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class loginModel {
    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("userDetail")
    @Expose
    private userDetailsModelLogin userDetail;

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public userDetailsModelLogin getUserDetail() {
        return userDetail;
    }

    public void setUserDetail(userDetailsModelLogin userDetail) {
        this.userDetail = userDetail;
    }

}

