package com.example.facitqroccupancy;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.facitqroccupancy.dataClass.qrData;
import com.google.gson.Gson;

public class Preferences {

    private static final String MY_PREFERENCES = "MY_SHARED_PREFERENCES";

    public static void setLogin(qrData model, Context context){
        SharedPreferences preferences = context.getSharedPreferences(MY_PREFERENCES,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("user", new Gson().toJson(model));
        editor.apply();
    }


}
